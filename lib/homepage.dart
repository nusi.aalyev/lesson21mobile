import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


class HomePage extends StatefulWidget{
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String statuscode = '';
  create() async{
    var response = await http
        .post(Uri.parse("http://192.168.88.253:8080/api/ec2/create"));
    setState(() {
      statuscode = "Create Instance!";
    });
  }
  terminate() async{
    var response = await http
        .post(Uri.parse("http://192.168.88.253:8080/api/ec2/terminate"));
    setState(() {
      statuscode = "Terminated Instance!";
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Task#21'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 80,
              width: 400,
              child: const Center(
                child: Text("Fisrt Flutter App",
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ),

            Container(
              height: 80,
              width: 200,
              child: FilledButton(
                onPressed: create,
                child: const Text(
                  "Создать",
                  style: TextStyle(
                      color: Colors.white
                  ),
                ),
              ),
            ),
            Container(
              height: 80,
              width: 200,
              child: FilledButton(
                onPressed: terminate,
                child: const Text(
                  "Удалить",
                  style: TextStyle(
                      color: Colors.white
                  ),
                ),
              ),
            ),
            Container(
              height: 80,
              width: 500,
              child: Center(
                child: Text('$statuscode',
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ),

          ],
        ),

      ),
    );
  }
}
